<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>17 POST Change fullname with alpha numeric symbol character</name>
   <tag></tag>
   <elementGuidId>58a7cb4e-64ed-4e81-bf70-3ad6093702b2</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <authorizationRequest>
      <authorizationInfo>
         <entry>
            <key>bearerToken</key>
            <value>eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiZGU4MWFjNDA5ZGE0MDU3MDJmMDQwYjJjNDNkMDA3OWI1MDc2MTNhNjhiYTQwYjI2YzU0NmMzZmJjYmYyNjRhNDQwOWFhNmY2MDc1ZDM5YTEiLCJpYXQiOjE2ODQ0MjU5MjYuODMyMjgxLCJuYmYiOjE2ODQ0MjU5MjYuODMyMjg0LCJleHAiOjE3MTYwNDgzMjYuODIwMzI4LCJzdWIiOiI0MzEiLCJzY29wZXMiOltdfQ.ISorKq12k8WybBBtfxXPrSr4EYGf77MwIpnEpID_ODqOB4Abw3-CmkVULonoBIIkXj-_tQXzXm9-UZ6mpq-y38pbwHESW2ues2tqPsSR4Q3PLgBztFAcv-RzyMPQZ23xLJQE3Mc6xyzNHBliXyeT4M7TpcAT4N5EY-hJrR6hHD0LJrzJzI2xRgcZw03QCAkkszrGhUaOAwFYW5wcI_yIXHjQ45yCog_vinBZTZjwHIl-yuUi39KemqkkCb4XZzsWpxq9KyJbkTIZVGDv8858xNt5vX6AzsxFenNvCM-o6k35eXI3xbhLcJ8N9LvbSe66m7Utufdr3_2k-nuOwjxG_w7NbpNehdRhBgjduKGxl1jSN7iRRSnByvvC1SZ9fIrkNU4HXArTbRllaxGWEjwYOzFEcaOeF0_NgY2k1NVf6Izmt_FtKl3ddSKt75sHZyf29WALb0YvYVv0nvNWahKiuW05kgdNe3APWukI_jqgFLZg--2zYelFk5VBfCZa4gg16zhmeBobesprA3k81BlGpYU9F11FuRGAdZ4SYIiAVXWMmnKUq6xCwTbSFpndmCAQGHsjpCdMi7IjgeGcIaWGyjyqoiXzs2oQvtnJ6ZYv_gZ0D4YE793gDmvCwwDMAaAmNgNpkN92g_nZL2G9iMw5qNBjigHwRnjjggmbQyEwvn0</value>
         </entry>
      </authorizationInfo>
      <authorizationType>Bearer</authorizationType>
   </authorizationRequest>
   <autoUpdateContent>false</autoUpdateContent>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;multipart/form-data&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;name&quot;,
      &quot;value&quot;: &quot;abcd12345!@#$%&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>form-data</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>multipart/form-data</value>
      <webElementGuid>b7191bec-098b-4fdb-8b9b-e5b8cff06de5</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiZGU4MWFjNDA5ZGE0MDU3MDJmMDQwYjJjNDNkMDA3OWI1MDc2MTNhNjhiYTQwYjI2YzU0NmMzZmJjYmYyNjRhNDQwOWFhNmY2MDc1ZDM5YTEiLCJpYXQiOjE2ODQ0MjU5MjYuODMyMjgxLCJuYmYiOjE2ODQ0MjU5MjYuODMyMjg0LCJleHAiOjE3MTYwNDgzMjYuODIwMzI4LCJzdWIiOiI0MzEiLCJzY29wZXMiOltdfQ.ISorKq12k8WybBBtfxXPrSr4EYGf77MwIpnEpID_ODqOB4Abw3-CmkVULonoBIIkXj-_tQXzXm9-UZ6mpq-y38pbwHESW2ues2tqPsSR4Q3PLgBztFAcv-RzyMPQZ23xLJQE3Mc6xyzNHBliXyeT4M7TpcAT4N5EY-hJrR6hHD0LJrzJzI2xRgcZw03QCAkkszrGhUaOAwFYW5wcI_yIXHjQ45yCog_vinBZTZjwHIl-yuUi39KemqkkCb4XZzsWpxq9KyJbkTIZVGDv8858xNt5vX6AzsxFenNvCM-o6k35eXI3xbhLcJ8N9LvbSe66m7Utufdr3_2k-nuOwjxG_w7NbpNehdRhBgjduKGxl1jSN7iRRSnByvvC1SZ9fIrkNU4HXArTbRllaxGWEjwYOzFEcaOeF0_NgY2k1NVf6Izmt_FtKl3ddSKt75sHZyf29WALb0YvYVv0nvNWahKiuW05kgdNe3APWukI_jqgFLZg--2zYelFk5VBfCZa4gg16zhmeBobesprA3k81BlGpYU9F11FuRGAdZ4SYIiAVXWMmnKUq6xCwTbSFpndmCAQGHsjpCdMi7IjgeGcIaWGyjyqoiXzs2oQvtnJ6ZYv_gZ0D4YE793gDmvCwwDMAaAmNgNpkN92g_nZL2G9iMw5qNBjigHwRnjjggmbQyEwvn0</value>
      <webElementGuid>62ff5352-0db3-4106-8b2c-159595c2539a</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.6.0</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://demo-app.online/api/updateprofile</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
