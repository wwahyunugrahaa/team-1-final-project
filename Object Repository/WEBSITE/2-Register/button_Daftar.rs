<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Daftar</name>
   <tag></tag>
   <elementGuidId>a6e4d483-83ef-461d-8f4d-b47cc7a511d5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#buttonRegisterTrack</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='buttonRegisterTrack']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>23cd11a6-0752-48d1-9f7b-955658b2c44f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>buttonRegisterTrack</value>
      <webElementGuid>aa5310e9-21a2-429b-80e9-66f356d78443</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>eb0f9127-37ed-4900-971d-770f41a16372</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-block btn-primary</value>
      <webElementGuid>fd7bbdfb-7908-46ee-aa48-e78dfb73fd12</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                            Daftar
                                        </value>
      <webElementGuid>a829fa27-055a-4f18-815f-c3a73a78a0e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;buttonRegisterTrack&quot;)</value>
      <webElementGuid>f33aa5b4-0503-4fad-bdfa-14980ec3c77f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='buttonRegisterTrack']</value>
      <webElementGuid>e3534eb2-6ddd-4f72-868e-d21a1ff436fc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='syarat dan ketentuan'])[1]/following::button[1]</value>
      <webElementGuid>afc40809-aada-4a9c-8809-ce92e2cee653</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Konfirmasi kata sandi'])[1]/following::button[1]</value>
      <webElementGuid>7eba9281-2851-44dc-8242-b9d9c543839f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Daftar']/parent::*</value>
      <webElementGuid>7e5d8af6-8dd1-42ac-935c-da8d75fcacd2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[8]/div/button</value>
      <webElementGuid>84bf2101-ec65-4bde-b0a0-84d9ae036373</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'buttonRegisterTrack' and @type = 'submit' and (text() = '
                                            Daftar
                                        ' or . = '
                                            Daftar
                                        ')]</value>
      <webElementGuid>cef1a163-7d9b-47ee-a756-9ccd735327f7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
