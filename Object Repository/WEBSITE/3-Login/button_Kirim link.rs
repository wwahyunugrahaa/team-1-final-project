<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Kirim link</name>
   <tag></tag>
   <elementGuidId>f459cb94-05e2-410d-8bc0-a938131d3308</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='btnCounter']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#btnCounter</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>2c619c2b-d788-4c05-9615-17d59ca7e762</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>2f3025ad-4a68-4121-9c5b-64fade967441</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>btnCounter</value>
      <webElementGuid>64c8f392-a5f8-429f-8f60-6ba34fa8f3c9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-block btn-primary</value>
      <webElementGuid>63d59f6f-9a61-44b8-b017-906fd773bb6b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                            Kirim link
                                        </value>
      <webElementGuid>1b609ace-0d8d-4efb-9204-34d1c4940bbb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;btnCounter&quot;)</value>
      <webElementGuid>f8f42e1e-e089-4a44-b75b-eb0a4c4fc140</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='btnCounter']</value>
      <webElementGuid>99ad0b04-94d6-4f4a-8d4c-b60fa54faae1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lupa Kata Sandi'])[1]/following::button[1]</value>
      <webElementGuid>c0d2ee61-de9f-400c-b7ae-4deba5243af0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='About CODING.ID'])[1]/preceding::button[1]</value>
      <webElementGuid>055c7423-6831-46f6-aac8-16a88408abdb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Site Map'])[1]/preceding::button[1]</value>
      <webElementGuid>56b3211a-477c-4721-8ebe-54ec86066280</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Kirim link']/parent::*</value>
      <webElementGuid>524c5f67-fd2b-4b36-8f86-eefd007526ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/button</value>
      <webElementGuid>bb78800d-00e0-405c-ae23-877b6c481872</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'submit' and @id = 'btnCounter' and (text() = '
                                            Kirim link
                                        ' or . = '
                                            Kirim link
                                        ')]</value>
      <webElementGuid>4acfa4bb-1a12-45d4-afd7-37eac277f591</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
