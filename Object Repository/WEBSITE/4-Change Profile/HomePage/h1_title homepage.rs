<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h1_title homepage</name>
   <tag></tag>
   <elementGuidId>598a033e-f5fa-4a44-897c-ba7f6cf87964</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h1.new_headline_h3</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-body-homepage']/div/h1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>b721519d-48b5-4c59-b76b-12a2a05fa806</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>new_headline_h3</value>
      <webElementGuid>1af3393f-4944-4dbd-99c7-cc98f3c0c50e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Coding Bootcamp
            Tech Talent Berkualitas</value>
      <webElementGuid>1f3aa2c3-7514-4d80-b0f7-3515ecbb7054</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-body-homepage&quot;)/div[@class=&quot;containerHeader&quot;]/h1[@class=&quot;new_headline_h3&quot;]</value>
      <webElementGuid>02c61b3b-c540-4ce8-b99b-4793b3fa2b5d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-body-homepage']/div/h1</value>
      <webElementGuid>7b51a573-d70d-4fb4-8d78-ff10aa73cae6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Quality Assurance Engineer Class'])[2]/preceding::h1[1]</value>
      <webElementGuid>d1adf1fb-a580-47ae-a310-151b39923999</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jadi QA Engineer hanya dalam 2 bulan dengan Jaminan Kerja dan Beasiswa*.'])[1]/preceding::h1[1]</value>
      <webElementGuid>b993b9c1-34e0-4633-a092-791d851c63ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Coding Bootcamp']/parent::*</value>
      <webElementGuid>a4d9c47e-5c5f-4049-9c12-36e73eb4275c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h1</value>
      <webElementGuid>aa0abe9c-2bf2-4533-aaf3-e581f81b722d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[(text() = 'Coding Bootcamp
            Tech Talent Berkualitas' or . = 'Coding Bootcamp
            Tech Talent Berkualitas')]</value>
      <webElementGuid>8856bfaa-4e51-404f-8a03-c54372e41328</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
