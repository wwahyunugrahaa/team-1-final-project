<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_change-photo</name>
   <tag></tag>
   <elementGuidId>377cf234-acbb-4152-a716-407cdc0e98f1</elementGuidId>
   <imagePath></imagePath>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div/div/div/form/div/figure/label/img</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>img.avatar-icon</value>
      </entry>
      <entry>
         <key>IMAGE</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@src = '/extra-images/icon/camera.png' and @alt = '...']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>d52c5fca-6909-4377-a9bb-85b41cda84f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>/extra-images/icon/camera.png</value>
      <webElementGuid>f8dea126-0261-4283-9e75-5db941fc752e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>avatar-icon</value>
      <webElementGuid>1120efe5-683a-4849-9084-fdc5d0905557</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>alt</name>
      <type>Main</type>
      <value>...</value>
      <webElementGuid>d3d887c5-7d07-4a44-be00-befab87ad643</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/div[@class=&quot;main-content&quot;]/section[@class=&quot;section&quot;]/div[@class=&quot;section-body&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;card author-box&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-12 col-md-12 col-lg-6&quot;]/div[@class=&quot;card-body&quot;]/form[1]/div[@class=&quot;author-box-center&quot;]/figure[@class=&quot;avatar mr-2 avatar-xl&quot;]/label[1]/img[@class=&quot;avatar-icon&quot;]</value>
      <webElementGuid>44795d12-653f-45b1-9169-b6e1efa609d1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div/div/div/form/div/figure/label/img</value>
      <webElementGuid>be2e90c7-4b57-46da-8755-8b31a7f0b9f7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:img</name>
      <type>Main</type>
      <value>//img[@alt='...']</value>
      <webElementGuid>0d558b5e-4bdd-43db-ad12-62f051292840</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//label/img</value>
      <webElementGuid>40fc5b4b-94f4-483a-8403-241213a36d02</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//img[@src = '/extra-images/icon/camera.png' and @alt = '...']</value>
      <webElementGuid>f18ddb2b-f1b1-4e96-85bd-0a88405b4e6e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
