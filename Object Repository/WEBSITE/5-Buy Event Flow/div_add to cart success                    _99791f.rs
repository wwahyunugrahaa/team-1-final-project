<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_add to cart success                    _99791f</name>
   <tag></tag>
   <elementGuidId>1a1ebd0c-8ebf-42bb-a1ff-08bf585c0e96</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='Modal_Success']/div/div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>3181d866-c63d-4060-a8ea-4f7b090fe528</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-body</value>
      <webElementGuid>70e534ce-fe1a-4ffb-9147-6f6a1d9de636</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            add to cart success
            
            Day 4: Workshop
            
            
            Lihat
                Pembelian Saya
            
            
            
            
            

             Lihat Event Lainnya

        </value>
      <webElementGuid>2e8a5f17-14f4-411c-be5f-7e0db9ebb727</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Modal_Success&quot;)/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-body&quot;]</value>
      <webElementGuid>5cc5ccbe-093a-47c6-9e7d-1b13050e513d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='Modal_Success']/div/div[2]</value>
      <webElementGuid>84d13ef2-67c6-405b-9e7f-758f58e7559c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tutup'])[2]/following::div[1]</value>
      <webElementGuid>b4bc8d46-91d0-419d-969c-3995b505ce97</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div/div[2]</value>
      <webElementGuid>469f0d2b-a0a5-4a7b-8ce9-6cf68e6b0e48</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
            add to cart success
            
            Day 4: Workshop
            
            
            Lihat
                Pembelian Saya
            
            
            
            
            

             Lihat Event Lainnya

        ' or . = '
            add to cart success
            
            Day 4: Workshop
            
            
            Lihat
                Pembelian Saya
            
            
            
            
            

             Lihat Event Lainnya

        ')]</value>
      <webElementGuid>6740d2a9-efa5-4d11-92c1-b667eeade32f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
