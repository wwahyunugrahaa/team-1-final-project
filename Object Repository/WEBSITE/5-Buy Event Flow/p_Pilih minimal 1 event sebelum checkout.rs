<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>p_Pilih minimal 1 event sebelum checkout</name>
   <tag></tag>
   <elementGuidId>7f7a7436-4a68-4abc-a83d-1ccf70589116</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='modalEvent']/div/div/div[2]/p</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.modal-body > p</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>148d6318-8248-40b8-bc38-183c75c67ed9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Pilih minimal 1 event sebelum checkout</value>
      <webElementGuid>b0b6f29f-8d09-40bb-9bf0-2531b86dddd9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;modalEvent&quot;)/div[@class=&quot;modal-dialog&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]/p[1]</value>
      <webElementGuid>c26142dd-a0e4-4bb0-9757-f561a1b76c73</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='modalEvent']/div/div/div[2]/p</value>
      <webElementGuid>7a8d942f-5cbf-4e94-be03-9141ab000709</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Information'])[1]/following::p[1]</value>
      <webElementGuid>0f3c6784-039c-477c-a987-522941140239</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Confirm'])[1]/following::p[1]</value>
      <webElementGuid>6d4345aa-eb80-4fe8-a711-372da9fcd862</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ok'])[1]/preceding::p[1]</value>
      <webElementGuid>67ca0562-144f-4030-ae0c-9ab9b070d542</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Copyright © 2010-2021 Coding.ID All rights reserved.'])[1]/preceding::p[1]</value>
      <webElementGuid>a3ade7db-fc3c-4587-b33d-02eb445715ba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Pilih minimal 1 event sebelum checkout']/parent::*</value>
      <webElementGuid>45ef5b2d-3bad-42c8-bea3-ba7524f760d1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div[2]/p</value>
      <webElementGuid>3b385232-f180-42a7-aa8a-2bfb20217b16</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Pilih minimal 1 event sebelum checkout' or . = 'Pilih minimal 1 event sebelum checkout')]</value>
      <webElementGuid>0c5f3af4-6cd3-4361-b0e9-fdf8bb1424d9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
