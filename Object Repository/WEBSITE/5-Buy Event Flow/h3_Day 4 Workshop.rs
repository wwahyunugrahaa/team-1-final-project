<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h3_Day 4 Workshop</name>
   <tag></tag>
   <elementGuidId>85fbfa06-e682-4cbb-a7a1-9954fefb1ed0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='cartForm']/div/div[2]/div[2]/div/h3</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
      <webElementGuid>b3bc5365-5b83-48c9-ad8a-2253af0f246b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                    Day 4: Workshop</value>
      <webElementGuid>44b4e39b-8aba-43da-baa6-7eb9bad5f263</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;cartForm&quot;)/div[@class=&quot;col-md-8&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-7&quot;]/div[1]/h3[1]</value>
      <webElementGuid>1ee19195-9374-42b9-bb21-bf67590b2848</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='cartForm']/div/div[2]/div[2]/div/h3</value>
      <webElementGuid>0fd788d8-6e17-4b38-b9a6-d7e32564a367</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Event'])[1]/following::h3[2]</value>
      <webElementGuid>a84838ef-7c88-407c-98a1-7abf4e0eca45</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Event'])[2]/preceding::h3[1]</value>
      <webElementGuid>06ba4afd-2460-447c-ae0e-42b1d07a7a0a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Day 4: Workshop']/parent::*</value>
      <webElementGuid>5bef2ed1-1325-4295-9500-7352aeeb2537</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/h3</value>
      <webElementGuid>c472ef82-3059-4ba0-a42c-175243213bfc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[(text() = '
                                                    Day 4: Workshop' or . = '
                                                    Day 4: Workshop')]</value>
      <webElementGuid>0a62a749-ad30-43c7-aa35-ecb6ea8fd39d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
