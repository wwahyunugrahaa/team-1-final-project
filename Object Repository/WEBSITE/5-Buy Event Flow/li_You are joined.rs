<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_You are joined</name>
   <tag></tag>
   <elementGuidId>b158251f-4259-492d-a53c-7074e1155337</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='list-button']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#list-button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>a8da61e6-6fb3-4463-8af9-8fe15e3fa563</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>list-button</value>
      <webElementGuid>e202bbaa-e8b3-4844-b04a-1928d643c143</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                                                                
                                                                                                                        
                                                You are joined
                                        
                                                                    

                                                        

                            
                        </value>
      <webElementGuid>1cb81cd0-8e49-44e3-a1b6-f407c73bbd48</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;list-button&quot;)</value>
      <webElementGuid>e55c4640-92ec-45e4-8e3f-3d52f5e68b81</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//li[@id='list-button']</value>
      <webElementGuid>25216266-be9e-4fb2-9c16-d3a06e8e8fcb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Detik'])[1]/following::li[1]</value>
      <webElementGuid>a2ed208b-c46f-43f9-bc57-b38ad3336260</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Menit'])[1]/following::li[1]</value>
      <webElementGuid>13a7a563-3d16-4cab-b712-c5eda1b86eb7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Event lain yang banyak diminati'])[1]/preceding::li[1]</value>
      <webElementGuid>142dba43-75df-43a0-bdc5-6858b6f29d86</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//aside/div[2]/ul/li[6]</value>
      <webElementGuid>0d3f43b3-0155-432a-8db6-7e405fd949bb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[@id = 'list-button' and (text() = '
                                                                                                
                                                                                                                        
                                                You are joined
                                        
                                                                    

                                                        

                            
                        ' or . = '
                                                                                                
                                                                                                                        
                                                You are joined
                                        
                                                                    

                                                        

                            
                        ')]</value>
      <webElementGuid>ce1c111b-ab72-4048-a288-58705aec7c4e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
