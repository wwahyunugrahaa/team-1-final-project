<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_burger</name>
   <tag></tag>
   <elementGuidId>811361d2-e609-49e5-9bce-c0a6c8f263f6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/nav/div/ul/li/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.nav-link.nav-link-lg.collapse-btn</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>93a96a5d-7b5e-4b4b-9b04-450db522cea6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#</value>
      <webElementGuid>a41384d8-8883-4155-b0aa-2217c4ab51dd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>sidebar</value>
      <webElementGuid>839c7a42-4d6a-412c-affc-b1a4c6292726</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>nav-link nav-link-lg
									collapse-btn</value>
      <webElementGuid>df0a9104-e2ba-4b6e-a5ad-2bea85364b53</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/nav[@class=&quot;navbar navbar-expand-lg main-navbar sticky&quot;]/div[@class=&quot;form-inline mr-auto&quot;]/ul[@class=&quot;navbar-nav mr-3&quot;]/li[1]/a[@class=&quot;nav-link nav-link-lg
									collapse-btn&quot;]</value>
      <webElementGuid>03c9ef3b-a1b5-460c-9b39-36769c004e3f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/nav/div/ul/li/a</value>
      <webElementGuid>3bc790dc-a6a6-4667-b828-fc981f3e7a47</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mark All As Read'])[1]/preceding::a[2]</value>
      <webElementGuid>db49f887-0d02-45e7-b52b-2a0719cdd5b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '#')]</value>
      <webElementGuid>7afaf43f-1181-434b-90ec-1e4385633013</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a</value>
      <webElementGuid>f0087e37-ac7e-4a8f-b0b2-7d72ed8646ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '#']</value>
      <webElementGuid>9b1b44a2-5fa9-414c-85b9-0f30fe937ee0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
