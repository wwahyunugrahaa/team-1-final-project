<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Toggle navigation</name>
   <tag></tag>
   <elementGuidId>0c102acd-08ce-4161-a4e5-489f1577aa15</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@type='button']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button.navbar-toggle.collapsed</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>f73d232f-edbb-4123-8e34-e71ada74b7f1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>daa85acc-d468-4c1f-a4de-2bc88725d080</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>modal</value>
      <webElementGuid>74d09436-6150-4713-b8f1-4afa37cbc513</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-target</name>
      <type>Main</type>
      <value>#Modal_user</value>
      <webElementGuid>288340ad-58b9-41be-9cfe-fa895b3c6856</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>navbar-toggle collapsed</value>
      <webElementGuid>ab0ab647-6bf2-4ef9-babc-dc603c744668</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>eb00c6a9-7551-4c5a-be8d-87b8614247ea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        
                                        Toggle navigation
                                        
                                        
                                        
                                        
                                    </value>
      <webElementGuid>f2263975-d229-46cd-b91d-2459287069ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbar-col&quot;)/div[@class=&quot;wm-right-section&quot;]/nav[@class=&quot;navbar navbar-default&quot;]/div[@class=&quot;navbar-header&quot;]/button[@class=&quot;navbar-toggle collapsed&quot;]</value>
      <webElementGuid>244f69e2-70e3-4a6a-893a-7c5fe1334b10</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@type='button']</value>
      <webElementGuid>0d190f98-45b5-42a8-961d-e8e17bb39d48</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbar-col']/div/nav/div/button</value>
      <webElementGuid>b237c314-a5c8-48ed-a902-79777e26126c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bootcamp'])[1]/preceding::button[3]</value>
      <webElementGuid>617ad323-ee04-4cf3-a1a0-ed4da2735f92</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button</value>
      <webElementGuid>6ec2f11d-1812-4311-9eb6-3ee09a4bb377</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = '
                                        
                                        Toggle navigation
                                        
                                        
                                        
                                        
                                    ' or . = '
                                        
                                        Toggle navigation
                                        
                                        
                                        
                                        
                                    ')]</value>
      <webElementGuid>e6508f7a-edb5-404e-a1db-e1b67397574c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
