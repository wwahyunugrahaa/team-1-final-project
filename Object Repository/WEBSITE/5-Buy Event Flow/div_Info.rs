<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Info</name>
   <tag></tag>
   <elementGuidId>2590b656-066a-4305-84d5-1214b613eb53</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='modalRespon']/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.modal-content > div.modal-header</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>9d9cdc0e-b2de-42d0-9773-799df1f37b9c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-header</value>
      <webElementGuid>41c8a433-e4ad-4172-a709-1298371ff486</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                Info
                
                    ×
                
            </value>
      <webElementGuid>685409aa-c7e8-4656-a494-0c971af998c2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;modalRespon&quot;)/div[@class=&quot;modal-dialog&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-header&quot;]</value>
      <webElementGuid>f4ff5203-fe0e-444a-8736-b28f4c7395a7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='modalRespon']/div/div/div</value>
      <webElementGuid>064daa35-fe51-42d5-b21b-2b4a26437c7c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Format email tidak valid'])[1]/following::div[4]</value>
      <webElementGuid>92a3c194-dc51-4e61-9e9e-25210219c91b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Close'])[1]/preceding::div[2]</value>
      <webElementGuid>a24528d2-c201-4e1b-b9c5-952dfc4fe46f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/div</value>
      <webElementGuid>c7c36c94-356b-4d9b-a74c-ceb4c0dce60d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                Info
                
                    ×
                
            ' or . = '
                Info
                
                    ×
                
            ')]</value>
      <webElementGuid>37899850-935f-4a23-8c89-5a7416ddf98c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
