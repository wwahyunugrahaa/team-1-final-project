<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Selamat, anda telah menjadi subscriber _9ef126</name>
   <tag></tag>
   <elementGuidId>d01694e7-9ba1-42a2-8812-8660fe3dc1bc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='modalRespon']/div/div/div[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.modal-content > div.modal-body</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>a0649e17-4d16-4ff3-b042-9b477e130ebd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-body</value>
      <webElementGuid>26d1b730-35e0-469a-a4b8-8f3726377a02</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                Selamat, anda telah menjadi subscriber Coding.ID.
            </value>
      <webElementGuid>b444baf5-76ca-4b37-9f1d-3ca727ffc7e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;modalRespon&quot;)/div[@class=&quot;modal-dialog&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]</value>
      <webElementGuid>22b4c1a9-fc22-4cbd-830a-5f9cfc0f3fa5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='modalRespon']/div/div/div[2]</value>
      <webElementGuid>e89a3c1e-ce79-45e9-9cda-f7a084a5d682</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Info'])[1]/following::div[1]</value>
      <webElementGuid>359eaa56-e224-4358-97ae-e48ce521d074</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Close'])[1]/preceding::div[1]</value>
      <webElementGuid>35a7a151-b0f7-48fc-8e71-7d609573047b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='About CODING.ID'])[1]/preceding::div[2]</value>
      <webElementGuid>41951189-82d2-46c1-9fd0-c4627013cff6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/div[2]</value>
      <webElementGuid>45eac804-964e-4d11-a225-0bd4aa6cc3b1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                Selamat, anda telah menjadi subscriber Coding.ID.
            ' or . = '
                Selamat, anda telah menjadi subscriber Coding.ID.
            ')]</value>
      <webElementGuid>c1b595f5-8268-4da3-87c5-819947890c7d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
