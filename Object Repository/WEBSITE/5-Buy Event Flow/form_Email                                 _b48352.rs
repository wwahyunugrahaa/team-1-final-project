<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>form_Email                                 _b48352</name>
   <tag></tag>
   <elementGuidId>e2cd8714-3d58-4958-866f-3dc52bce0c11</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@action='https://demo-app.online/login']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.card-body > form</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>form</value>
      <webElementGuid>8bc2bbc5-06e2-46b1-9094-410a10037691</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>method</name>
      <type>Main</type>
      <value>POST</value>
      <webElementGuid>9c52a823-4401-49dd-a64d-51de7a4c2779</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>action</name>
      <type>Main</type>
      <value>https://demo-app.online/login</value>
      <webElementGuid>36c55c15-a280-4c40-a28f-b5ac95066968</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                
                                                
                                                    Email
                                                    
                                                                                                            
                                                            Email atau kata sandi salah
                                                        
                                                                                                    
                                                
                                                    
                                                        Kata
                                                            Sandi

                                                    
                                                    
                                                                                                    
                                                
                                                
                                                    
                                                        Login
                                                    
                                                
                                                
                                                
                                                     
                                                            
                                                            Sign-in With Google
                                                        
                                                
                                                                                            </value>
      <webElementGuid>2273978b-3814-4068-b700-2f2f096efa40</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;no-htmlimports no-flash no-proximity no-applicationcache blobconstructor blob-constructor cookies cors customprotocolhandler dataview eventlistener geolocation history no-ie8compat json notification queryselector serviceworker customevent postmessage svg templatestrings typedarrays websockets no-xdomainrequest webaudio webworkers no-contextmenu cssall audio canvas canvastext contenteditable emoji olreversed no-userdata video no-vml webanimations webgl adownload audioloop canvasblending todataurljpeg todataurlpng todataurlwebp canvaswinding no-ambientlight hashchange inputsearchevent pointerevents no-hiddenscroll mathml unicoderange no-touchevents no-unicode no-batteryapi no-battery-api crypto no-dart gamepads fullscreen indexeddb indexeddb-deletedatabase intl pagevisibility performance pointerlock quotamanagement requestanimationframe raf vibrate no-webintents no-lowbattery getrandomvalues backgroundblendmode cssanimations backdropfilter backgroundcliptext appearance exiforientation audiopreload&quot;]/body[1]/div[@class=&quot;wm-main-wrapper&quot;]/div[@class=&quot;wm-main-section&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-md-12 col-sm-12 col-xs-12&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-md-4&quot;]/div[@class=&quot;card card-primary&quot;]/div[@class=&quot;card-body&quot;]/form[1]</value>
      <webElementGuid>c8f729be-ce21-4144-93b2-842e866e9dd0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//form[@action='https://demo-app.online/login']</value>
      <webElementGuid>10cec930-329e-4717-8524-35d3edebf782</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Masuk'])[3]/following::form[1]</value>
      <webElementGuid>da436c4f-8f40-4449-8837-19805800224a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/form</value>
      <webElementGuid>fec92f4e-5146-4ac3-a636-4784f107566d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//form[(text() = '
                                                
                                                
                                                    Email
                                                    
                                                                                                            
                                                            Email atau kata sandi salah
                                                        
                                                                                                    
                                                
                                                    
                                                        Kata
                                                            Sandi

                                                    
                                                    
                                                                                                    
                                                
                                                
                                                    
                                                        Login
                                                    
                                                
                                                
                                                
                                                     
                                                            
                                                            Sign-in With Google
                                                        
                                                
                                                                                            ' or . = '
                                                
                                                
                                                    Email
                                                    
                                                                                                            
                                                            Email atau kata sandi salah
                                                        
                                                                                                    
                                                
                                                    
                                                        Kata
                                                            Sandi

                                                    
                                                    
                                                                                                    
                                                
                                                
                                                    
                                                        Login
                                                    
                                                
                                                
                                                
                                                     
                                                            
                                                            Sign-in With Google
                                                        
                                                
                                                                                            ')]</value>
      <webElementGuid>8b693cda-5821-4b3f-8454-1825fe9ed834</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
