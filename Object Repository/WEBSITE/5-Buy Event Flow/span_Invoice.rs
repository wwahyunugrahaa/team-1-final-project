<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Invoice</name>
   <tag></tag>
   <elementGuidId>5a7fe813-424b-4049-ae17-7c8f8b778d13</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//aside[@id='sidebar-wrapper']/ul/li[10]/a/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>b00ae5e7-688f-4c35-b9cf-a0d599d6caf5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Invoice</value>
      <webElementGuid>3569f87d-17d0-49c6-a539-b719d2c2bf10</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;sidebar-wrapper&quot;)/ul[@class=&quot;sidebar-menu&quot;]/li[@class=&quot;dropdown&quot;]/a[@class=&quot;nav-link&quot;]/span[1]</value>
      <webElementGuid>eae3592d-5058-48b6-9f11-3d6a72a5dc3e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//aside[@id='sidebar-wrapper']/ul/li[10]/a/span</value>
      <webElementGuid>fc8c111d-5c6c-4d10-b545-ff9d0dd547ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My points'])[1]/preceding::span[1]</value>
      <webElementGuid>734b8f57-0dd3-4787-bec4-864d0ccc532c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My Events'])[2]/preceding::span[1]</value>
      <webElementGuid>c54d7777-00f1-41c7-89a4-30c1eb139417</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Invoice']/parent::*</value>
      <webElementGuid>336aa8de-0dc5-4b6f-aa08-bac065ad85e6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[10]/a/span</value>
      <webElementGuid>2032e15a-6d8b-46e8-847f-85d2505f2d63</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Invoice' or . = 'Invoice')]</value>
      <webElementGuid>fec68af5-5b5d-4031-9a4c-cbf179e7f54c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
