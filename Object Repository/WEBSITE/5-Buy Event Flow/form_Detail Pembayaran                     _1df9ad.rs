<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>form_Detail Pembayaran                     _1df9ad</name>
   <tag></tag>
   <elementGuidId>c8a6dfe2-b464-4e06-a464-612eab39eae6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#modalform</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>form</value>
      <webElementGuid>134a0e09-b3ed-4040-9426-9aec7a249fcd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>action</name>
      <type>Main</type>
      <value>/view_cart/invoice</value>
      <webElementGuid>cb5636d9-6d26-4340-aeb9-337223867ec2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>method</name>
      <type>Main</type>
      <value>post</value>
      <webElementGuid>45bbaf33-39c6-48ac-b6af-10f7412e59a5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>modalform</value>
      <webElementGuid>82557df4-b6bf-4e83-9688-30e40caa35bc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>                                Detail Pembayaran 
                                

                                    
                                        
                                            Total Price
                                        
                                        85.000

                                    
                                    
                                        
                                            Discount Referral / Voucher
                                        
                                        -
                                        
                                    
                                    
                                        
                                            Point Used
                                        
                                        -
                                        
                                    
                                    
                                        
                                            Total Pembayaran
                                        
                                        
                                            85.000
                                        

                                    
                                
                                 Pilih Metode Pembayaran
                                
                                    
                                        
                                            
                                        
                                        
                                            Transfer QR Code / Bank (cek otomatis)
                                        

                                    

                                    
                                        
                                            
                                        
                                        
                                            Sisa Points 
                                                0
                                            
                                        


                                    
                                    
                                        
                                        *Jika poin tidak mencukupi maka sisa pembayaran
                                                dilakukan melalui transfer melalui QR Code
                                        
                                    
                                
                                


                                Confirm
                            </value>
      <webElementGuid>b6d45865-4909-4e7e-a134-40c8674f83d2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;modalform&quot;)</value>
      <webElementGuid>ab39c2c8-383d-4ea8-939c-dea89555fe80</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//form[@id='modalform']</value>
      <webElementGuid>07d1018d-1024-470d-b589-85e3892a9c93</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='ModalLogin']/div/div/div/div/form</value>
      <webElementGuid>58d4a271-6760-4417-a754-1214c79e20bf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='×'])[1]/following::form[1]</value>
      <webElementGuid>3b874b86-1071-4fe0-81dd-13c8885c1c30</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div/form</value>
      <webElementGuid>c26be86b-7284-457b-8ffd-49ad77b3ec30</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//form[@id = 'modalform' and (text() = '                                Detail Pembayaran 
                                

                                    
                                        
                                            Total Price
                                        
                                        85.000

                                    
                                    
                                        
                                            Discount Referral / Voucher
                                        
                                        -
                                        
                                    
                                    
                                        
                                            Point Used
                                        
                                        -
                                        
                                    
                                    
                                        
                                            Total Pembayaran
                                        
                                        
                                            85.000
                                        

                                    
                                
                                 Pilih Metode Pembayaran
                                
                                    
                                        
                                            
                                        
                                        
                                            Transfer QR Code / Bank (cek otomatis)
                                        

                                    

                                    
                                        
                                            
                                        
                                        
                                            Sisa Points 
                                                0
                                            
                                        


                                    
                                    
                                        
                                        *Jika poin tidak mencukupi maka sisa pembayaran
                                                dilakukan melalui transfer melalui QR Code
                                        
                                    
                                
                                


                                Confirm
                            ' or . = '                                Detail Pembayaran 
                                

                                    
                                        
                                            Total Price
                                        
                                        85.000

                                    
                                    
                                        
                                            Discount Referral / Voucher
                                        
                                        -
                                        
                                    
                                    
                                        
                                            Point Used
                                        
                                        -
                                        
                                    
                                    
                                        
                                            Total Pembayaran
                                        
                                        
                                            85.000
                                        

                                    
                                
                                 Pilih Metode Pembayaran
                                
                                    
                                        
                                            
                                        
                                        
                                            Transfer QR Code / Bank (cek otomatis)
                                        

                                    

                                    
                                        
                                            
                                        
                                        
                                            Sisa Points 
                                                0
                                            
                                        


                                    
                                    
                                        
                                        *Jika poin tidak mencukupi maka sisa pembayaran
                                                dilakukan melalui transfer melalui QR Code
                                        
                                    
                                
                                


                                Confirm
                            ')]</value>
      <webElementGuid>986225b6-7235-4d0b-b680-686f5f1b9d69</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
