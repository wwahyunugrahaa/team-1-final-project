<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Checkout</name>
   <tag></tag>
   <elementGuidId>53bb96a8-f621-4d45-8432-2fa7014dd2da</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='Modal_user']/div/div[2]/li[2]/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.modal-body > li:nth-of-type(2) > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>42c8a5ee-2490-4396-b46d-ae356d8c1b88</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/view_cart</value>
      <webElementGuid>19292c1d-257d-4242-8da4-57b100d39709</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Checkout</value>
      <webElementGuid>d8225064-b162-493f-90f7-542b47b01c6b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Modal_user&quot;)/div[@class=&quot;modal-dialog modal-lg&quot;]/div[@class=&quot;modal-body&quot;]/li[2]/a[1]</value>
      <webElementGuid>ff548685-83c9-45e3-ba94-d0685516c5df</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='Modal_user']/div/div[2]/li[2]/a</value>
      <webElementGuid>eac512a9-b977-40b0-8a9a-40acb2609c58</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>(//a[contains(text(),'Checkout')])[2]</value>
      <webElementGuid>6cb0b6e5-7116-4998-ba6f-a4aa399fb104</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My Course'])[2]/following::a[1]</value>
      <webElementGuid>4d49df63-a1c6-4e28-8375-5727f501fca6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='X'])[1]/following::a[2]</value>
      <webElementGuid>fca12933-a4bc-4b31-9ee7-b1f6fee00ee4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My Account'])[2]/preceding::a[1]</value>
      <webElementGuid>f51de151-640e-47b9-a09b-88ce7874986f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Logout'])[2]/preceding::a[2]</value>
      <webElementGuid>d9b0911b-39e5-4fa7-9ded-45b340a37e44</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '/view_cart')])[2]</value>
      <webElementGuid>d28d699b-c236-4303-95a0-81629a464a90</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/li[2]/a</value>
      <webElementGuid>d1d59ed8-f0bc-480a-b966-7329a9fa9be4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/view_cart' and (text() = 'Checkout' or . = 'Checkout')]</value>
      <webElementGuid>80d92311-8da9-43fd-99d8-c97bb7b2fa47</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
