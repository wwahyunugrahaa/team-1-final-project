<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>WTS02-Change Profile</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>39a81b47-5be2-4940-a59f-a807f2bbe9b8</testSuiteGuid>
   <testCaseLink>
      <guid>df8c8962-7ff8-41b9-961a-427244ecfa42</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/3-Change Profile/1 Positive/WEBTCCP001-Change fullname with alphabet characters</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>38cd5f0a-8239-4854-b4ad-70f604e3b398</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/3-Change Profile/1 Positive/WEBTCCP002-Change photo</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>00b2ed36-270b-49ac-9f86-def666d8f65d</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/data image</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>00b2ed36-270b-49ac-9f86-def666d8f65d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>location</value>
         <variableId>10d4059d-df60-4eda-9fb4-5daf23518426</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>e55c1ce2-5c1e-41d9-a58a-e0daf81bc684</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/3-Change Profile/1 Positive/WEBTCCP003-Change phone number between 10-12 characters</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>fbf78276-40d1-45bc-bd43-2f4fe7a220e0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/3-Change Profile/1 Positive/WEBTCCP004-Change birth-date in age above 6 years old</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>2889fe0c-53c2-4d3f-b06e-94e300383068</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/3-Change Profile/1 Positive/WEBTCCP015-verify phoneNumber in profile page match with editProfile Page</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>06f0bd13-1115-46e0-a7df-2db87e4043c5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/3-Change Profile/1 Positive/WEBTCCP016-verify birthDate in profile page match with editProfile Page</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>b7a3cfa6-c91c-468f-9929-a164ab7d25cc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/3-Change Profile/1 Positive/WEBTCCP017-verify fullName in profile page match with editProfile Page</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>db9a9b30-deb0-4044-945a-0cbb0e3c90d6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/3-Change Profile/1 Positive/WEBTCCP018-Didnt edit profile</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>f4bdde12-f8fa-4bdf-a5ce-b9eab167b179</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/3-Change Profile/2 Negative/WEBTCCP006-Change age under 6 years old</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>318c6b59-8e88-4f8c-b528-9e9ab2d249e0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/3-Change Profile/2 Negative/WEBTCCP007-Change email</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>27bffd8b-cd79-4e34-be8d-5e3a0620ff81</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/3-Change Profile/2 Negative/WEBTCCP009-Change whatsapp number less than 10 characters</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>3d2b406b-bae9-4a40-9d06-a339f20b9a6c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/3-Change Profile/2 Negative/WEBTCCP010-Change whatsapp number more than 12 characters</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>d3f84fa8-a6f7-44c5-8957-981edf7cd79f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/3-Change Profile/2 Negative/WEBTCCP011-Change whatsapp number with alphabet characters</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>7a1c9a3a-3388-4ad5-885e-a7b2c6bfbf51</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/3-Change Profile/2 Negative/WEBTCCP012-Change whatsapp number with symbol characters</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>465efc9b-a27d-4de7-a207-1f6d6ba2ef30</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/3-Change Profile/2 Negative/WEBTCCP23-Change name but didnt save it</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>e22d3360-dfb8-46c2-b7cd-ccd16b08bec8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/3-Change Profile/2 Negative/WEBTCCP24-Change birth date but didnt save it</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>11166ea3-a06d-4d1e-b092-301d5111a5ad</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/3-Change Profile/2 Negative/WEBTCCP25-Change phone number but didnt save it</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
