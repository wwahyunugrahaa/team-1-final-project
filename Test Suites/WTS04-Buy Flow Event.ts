<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>WTS04-Buy Flow Event</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>2797f7b9-29c5-4a0c-b967-c17432d2699a</testSuiteGuid>
   <testCaseLink>
      <guid>e89317f5-c3ee-43ea-8703-e5c76ca3f996</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/4-Buy Event Flow/Negative/WEBTCBEF006-Purchased before logging in</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>fa1483de-2fb0-4445-9620-70b74a2b891a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/4-Buy Event Flow/Negative/WEBTCBEF007-Purchased an event that is close</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>ea56ab33-75a7-47d7-b7c6-6565e0c6d291</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/4-Buy Event Flow/Negative/WEBTCBEF008-Purchased an event that has already been join</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>03d4f47a-b30b-4df7-a03a-803c397316cb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/4-Buy Event Flow/Negative/WEBTCBEF009-Purchased an event without selecting a payment method</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>e8b8eb4f-0be0-4626-be45-48f87d60b4d1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/4-Buy Event Flow/Negative/WEBTCBEF010-Purchased an event without selecting the event</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>34cbd77b-0efa-47c4-9711-05e781e31a5f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/4-Buy Event Flow/Negative/WEBTCBEF011-Added the same event to the cart</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>41edf05e-f236-4efb-a203-4938753b4604</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/4-Buy Event Flow/Negative/WEBTCBEF014-Removed an event that was already in the checkout page</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>d3797904-1e6b-4871-b410-947b3e0f51d2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/4-Buy Event Flow/Negative/WEBTCBEF016-Enter invalid data for the referral code</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>49aa69e0-40ef-40a7-aa66-7a8bf1061880</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/4-Buy Event Flow/Negative/WEBTCBEF017-Enter empty data for the referral code</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>112d477a-8b14-47cd-8d8c-555d60feb4a2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/4-Buy Event Flow/Positive/WEBTCBEF002-Searched event with search feature</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>91c789fd-adab-4a7b-b701-a8a828705065</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/4-Buy Event Flow/Positive/WEBTCBEF005-Newsletter</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>d501b8a6-99cd-4015-9129-48d1a0f4aa24</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/4-Buy Event Flow/Positive/WEBTCBEF004-Muat lebih banyak</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>fff3a0d7-8709-41ac-ae04-a6f4d4abb663</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/4-Buy Event Flow/Positive/WEBTCBEF003-Viewed event detail</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>86a56d14-5e3f-4111-afad-551d3035af28</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/4-Buy Event Flow/Positive/WEBTCBEF012-Viewed other event with lihat event lainnya in the cart</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>563e14c5-fa4f-4373-b83b-07490be061a6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/4-Buy Event Flow/Positive/WEBTCBEF013-Viewed the checkout page</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>ad841cf6-7ebe-4a96-be64-b5afb6480b95</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/4-Buy Event Flow/Positive/WEBTCBEF015-Enter valid data for the referral code</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>bbd6cc32-ea9c-4abb-80f1-e81aa9373ce4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/4-Buy Event Flow/Positive/WEBTCBEF018-Invoice Menu</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>8f19b6c4-2a8a-4aa7-8643-258d32e56404</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/4-Buy Event Flow/Positive/WEBTCBEF020-Invoice search</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>c5921984-d7cc-42a5-9563-048b15631093</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/4-Buy Event Flow/Positive/WEBTCBEF021-Show detail invoice</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>0738e70b-e1a0-498d-bf7e-c73e4fed1d48</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/4-Buy Event Flow/Positive/WEBTCBEF022-Verified the funct next button</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>07bf0db7-2a1d-411a-a5ed-9a1661af3d83</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/WEBSITE/4-Buy Event Flow/Positive/WEBTCBEF023-Verified funct previous button</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
