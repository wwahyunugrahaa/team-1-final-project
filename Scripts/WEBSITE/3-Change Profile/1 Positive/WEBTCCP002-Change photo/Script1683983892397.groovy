import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo-app.online/login')

WebUI.setText(findTestObject('Object Repository/WEBSITE/4-Change Profile/record/input_Email_email'), 'khonsa.18015@mhs.unesa.ac.id')

WebUI.setEncryptedText(findTestObject('Object Repository/WEBSITE/4-Change Profile/record/input_Kata                                 _98da12'), 
    'RigbBhfdqODKcAsiUrg+1Q==')

WebUI.click(findTestObject('Object Repository/WEBSITE/4-Change Profile/record/button_Login'))

WebUI.click(findTestObject('WEBSITE/4-Change Profile/HomePage/btn_menu-profil'))

WebUI.click(findTestObject('WEBSITE/4-Change Profile/HomePage/btn_My Account in Menu'))

WebUI.click(findTestObject('Object Repository/WEBSITE/4-Change Profile/record/span_Profil'))

WebUI.click(findTestObject('Object Repository/WEBSITE/4-Change Profile/record/a_Edit Profile'))

WebUI.uploadFile(findTestObject('WEBSITE/4-Change Profile/Edit Profile Page/input_photo'), img)

WebUI.takeFullPageScreenshot()

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WEBSITE/4-Change Profile/record/button_Save Changes'))

WebUI.verifyElementText(findTestObject('WEBSITE/4-Change Profile/Profile Page/div_BerhasilNotif'), 'Berhasil')

WebUI.takeFullPageScreenshot(FailureHandling.STOP_ON_FAILURE)

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/WEBSITE/4-Change Profile/record/button_OK'))

WebUI.takeFullPageScreenshot()

WebUI.delay(2)

WebUI.closeBrowser()

