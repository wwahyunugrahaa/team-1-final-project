import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('WEBSITE/2-Login/Positive/WEBTCL001-Login'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.navigateToUrl('https://demo-app.online/event')

WebUI.click(findTestObject('Object Repository/WEBSITE/5-Buy Event Flow/div_Day 4 Workshop                         _31d43a'))

WebUI.delay(2)

WebUI.takeFullPageScreenshot()

WebUI.verifyElementVisible(findTestObject('Object Repository/WEBSITE/5-Buy Event Flow/a_Beli Tiket'))

WebUI.click(findTestObject('Object Repository/WEBSITE/5-Buy Event Flow/a_Beli Tiket'))

WebUI.verifyElementVisible(findTestObject('Object Repository/WEBSITE/5-Buy Event Flow/h3_add to cart success'))

WebUI.verifyElementVisible(findTestObject('Object Repository/WEBSITE/5-Buy Event Flow/a_Lihat                Pembelian Saya'))

WebUI.delay(2)

WebUI.takeFullPageScreenshot()

WebUI.click(findTestObject('Object Repository/WEBSITE/5-Buy Event Flow/a_Lihat                Pembelian Saya'))

WebUI.verifyElementVisible(findTestObject('Object Repository/WEBSITE/5-Buy Event Flow/h3_Day 4 Workshop'))

WebUI.verifyElementVisible(findTestObject('WEBSITE/5-Buy Event Flow/input_Checkout_referral_code'))

WebUI.delay(2)

WebUI.takeFullPageScreenshot()

WebUI.click(findTestObject('WEBSITE/5-Buy Event Flow/p_Remove'))

WebUI.delay(2)

WebUI.takeFullPageScreenshot()

WebUI.closeBrowser()

