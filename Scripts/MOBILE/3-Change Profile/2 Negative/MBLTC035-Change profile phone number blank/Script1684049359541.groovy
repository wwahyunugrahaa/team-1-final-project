import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('E:\\DemoAppV2.apk', true)

Mobile.tap(findTestObject('Mobile/3-Change Profile/btn_loginHere'), 0)

Mobile.setText(findTestObject('Mobile/3-Change Profile/editText_input-email'), 'khonsaazizaalmash@gmail.com', 0)

Mobile.setText(findTestObject('Mobile/3-Change Profile/editText_input-password'), '123456789', 0)

Mobile.tap(findTestObject('Mobile/3-Change Profile/btn_login'), 0)

Mobile.delay(20, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Mobile/3-Change Profile/btn_profilIcon'), 0)

Mobile.tap(findTestObject('Mobile/3-Change Profile/btn_setting'), 0)

Mobile.tap(findTestObject('Mobile/3-Change Profile/menu_editProfile'), 0)

Mobile.takeScreenshot('', FailureHandling.STOP_ON_FAILURE)

Mobile.delay(4, FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('Mobile/3-Change Profile/editText_noHP-1234567890'), '', 0)

Mobile.takeScreenshot('', FailureHandling.STOP_ON_FAILURE)

Mobile.delay(4, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementExist(findTestObject('Mobile/3-Change Profile/spanWarning_WhatsApp number is required'), 0)

Mobile.verifyElementAttributeValue(findTestObject('Mobile/3-Change Profile/btn_save-changes-disable'), 'enabled', 'false', 
    0)

Mobile.closeApplication()

