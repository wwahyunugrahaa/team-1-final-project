import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Users\\ASUS\\Downloads\\DemoAppV2.apk', true)

Mobile.tap(findTestObject('Object Repository/Mobile/1-Register/login_here_button'), 0)

Mobile.tap(findTestObject('Object Repository/Mobile/1-Register/Register_now_button'), 0)

Mobile.setText(findTestObject('Object Repository/Mobile/1-Register/name_field'), 'Mersi Suryani Siagian', 0)

Mobile.tap(findTestObject('Object Repository/Mobile/1-Register/set_date'), 0)

Mobile.tap(findTestObject('Object Repository/Mobile/1-Register/OKE_button'), 0)

Mobile.setText(findTestObject('Object Repository/Mobile/1-Register/email_field'), 'jhonraimonds90gmail.com', 0)

Mobile.setText(findTestObject('Object Repository/Mobile/1-Register/wa_field'), '081266558939', 0)

Mobile.setText(findTestObject('Object Repository/Mobile/1-Register/password_field'), 'Ramawaty123', 0)

Mobile.setText(findTestObject('Object Repository/Mobile/1-Register/conf_password'), 'Ramawaty123', 0)

Mobile.tap(findTestObject('Object Repository/Mobile/1-Register/CheckBox'), 0)

Mobile.tap(findTestObject('Object Repository/Mobile/1-Register/daftar_button'), 0)

Mobile.takeScreenshot()

